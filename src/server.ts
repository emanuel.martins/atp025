import express, { Application, NextFunction, Request, Response } from 'express';
import IPessoa from './interfaces/pessoa.interface';

const app: Application = express();
const port: number | string = process.env.PORT ?? 3333;

const pessoa: IPessoa = {
    nome: 'Pedro',
    sobrenome: 'Oliveira',
    idade: 18,
};

app.get('/', (_: Request, res: Response, next: NextFunction): Response => {
    return res.status(200).json({
        msg: 'Welcome to my node API',
        devMsg: 'Check /pessoa endpoint',
    });
});

app.get(
    '/pessoa',
    (_: Request, res: Response, next: NextFunction): Response => {
        return res.status(200).json(pessoa);
    }
);

app.listen(port, (): void =>
    console.log(`🔥 Server is running on port: ${port}`)
);
