export default interface IPessoa {
    nome: string;
    sobrenome: string;
    idade: number;
    email?: string;
}
